﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HomeworkWebBrowser
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            
        }

        private void AddPanelMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            WebBrowser webBrowser = new WebBrowser();
            Uri siteUri = new Uri("https://www.google.com/");
            // WebRequest webRequest = WebRequest.Create(siteUri);
            webBrowser.HorizontalAlignment = HorizontalAlignment.Stretch;
            webBrowser.VerticalAlignment = VerticalAlignment.Stretch;
            webBrowser.Source = siteUri;

            TabItem newTabItem = new TabItem
            {
                Header = "Google",
                Name = "newItem",
                Content = webBrowser
            };        
            TabControlCode.Items.Insert(1, newTabItem);
        }
    }
}
